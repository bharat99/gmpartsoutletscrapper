﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GmPartsOutletScrapper.GmPartsOutletBAL
{
    public class GmPartsOutletBussiness
    {
        private string ConGmPartsOutlet = ConfigurationSettings.AppSettings["GmPartsOutletconnectionString"].ToString();
        public DataTable ReturnDataTable(string query)
        {
            try
            {
                DataTable dt = new DataTable();
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    MySqlCommand cmd = new MySqlCommand(query);
                    cmd.Connection = con;
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                    con.Close();
                    return dt;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public DataTable GetParts()
        {
            DataTable dt = new DataTable();
            dt = ReturnDataTable("Select * from gmParts;");
            return dt;
        }

        public void InsertGmPartOutletLink(string partNumber,string partName, string url)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    string query = string.Format("INSERT INTO gmpartsoutletlink (PartNumber,PartType,Url,CreatedOn) VALUES('{0}','{1}','{2}',UTC_TIMESTAMP());",partNumber,partName,url);
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public DataTable GetGmPartsOutletLink(string PartsName, string gmpartsoutletlink)
        {
            DataTable dt = new DataTable();
            try
            {
                string query = string.Format("SELECT * FROM {1} WHERE PartName='{0}' AND IsProcessed=1;", PartsName,gmpartsoutletlink);
                dt = ReturnDataTable(query);
                return dt;
            }
            catch (Exception ex)
            {
                ErrorLog(ex, "PartName:" + PartsName + " TableName: " + gmpartsoutletlink, "", PartsName, "");
            }
            return dt;
        }

        public int InsertGmPartOutletData(string SKU,string Replaces,string imageUrl1,string imageUrl2,string imageUrl3,string imageUrl4,string imageUrl5,string imageUrl6,string PartNumber,int LinkID,string gmpartsoutletdata)
        {
            int DataID = 0;
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    string query = string.Format("INSERT INTO {10} (LinkID,PartNumber,SKU,Replaces,ImageUrl1, ImageUrl2, ImageUrl3,ImageUrl4, ImageUrl5, ImageUrl6,CreatedOn) VALUES({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',UTC_TIMESTAMP()); select last_insert_id(); UPDATE link_gmpartsoutlet SET IsProcessed=0 WHERE ID={0};", LinkID,PartNumber,SKU,Replaces,imageUrl1,imageUrl2,imageUrl3,imageUrl4,imageUrl5,imageUrl6, gmpartsoutletdata);
                    MySqlCommand cmd = new MySqlCommand(query, con);
                   DataID=Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    con.Close();
                    return DataID;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex,"LinkID: "+ LinkID, "InsertGmPartOutletData", "", PartNumber);
            }
            return DataID;
        }
        public void InsertVehicleFitment(string Make,string BodyAndTrim,string EngineAndTransmission,string PartNumber)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    string query = string.Format("INSERT INTO vehiclefitment (PartNumber,Make,BodyAndTrim,EngineAndTransmission,CreatedOn) VALUES('{0}','{1}','{2}','{3}',UTC_TIMESTAMP());", PartNumber, Make,BodyAndTrim, EngineAndTransmission);
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public int InsertBulkData(DataTable dtData)
        {
            try
            {

                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    //var bulk = new Z.BulkOperations.BulkOperation(con);
                    //bulk.BulkInsert(dtData);
                    //var bulkCopy = new MySqlBulkCopy(con);

                    //bulkCopy.WriteToServer(dtData);

                }
            }
            catch (Exception ex)
            {
                //CommonUtility.ErrorLog(ex, "", "InsertBulkData");
                return 0;
            }
            return 1;

        }
        public int  InsertBulkData(string path,string tableName)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    con.Open();
                    MySqlBulkLoader bLoad = new MySqlBulkLoader(con);

                    bLoad.TableName = tableName;
                    bLoad.FieldTerminator = "|";
                    bLoad.LineTerminator = "\n";
                    bLoad.FileName = path;
                    bLoad.NumberOfLinesToSkip = 0;

                    bLoad.Load();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex, "", "InsertBulkData", "", "");


            }
            return 0;
        }

        public void DeleteCSVFile(string FilePath)
        {
            try
            {
                if(System.IO.File.Exists(FilePath))
                {
                    System.IO.File.Delete(FilePath);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public void ErrorLog(Exception ex,string Url,string Functionname,string PartName,string PartNumber)
        {
            try
            {
                using (MySqlConnection con = new MySqlConnection(ConGmPartsOutlet))
                {
                    if (con.State != ConnectionState.Closed)
                    {
                        con.Close();
                    }
                    con.Open();
                    string query = string.Format("INSERT INTO ErrorLog (URL,FunctionName,ExceptionMessage,StackTrace,PartName,PartNumber,CreatedOn) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',UTC_TIMESTAMP());", Url, Functionname, ex.Message, ex.StackTrace, PartName, PartNumber);
                    MySqlCommand cmd = new MySqlCommand(query, con);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception exp)
            {
                throw;
            }
        }
    }
}

