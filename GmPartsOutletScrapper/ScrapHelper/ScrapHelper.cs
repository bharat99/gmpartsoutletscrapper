﻿using ScrapingHelp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GmPartsOutletScrapper.ScrapHelper
{
    public class ScrapHelper
    {
        int retryCount = 0;
        int ProxyChange = 0;
        public ScrapHelper(int _retryCount)
        {
            retryCount = _retryCount;
        }
        public string GetSource(string url, string scookie = "")
        {
            string Content = String.Empty;
            int retry = 0;
            string myPageSource = string.Empty;
        retryContent:
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                ServicePointManager.Expect100Continue = false;
                ServicePointManager.MaxServicePointIdleTime = 0;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;

                if (Convert.ToBoolean(ConfigurationSettings.AppSettings["_proxy"].Trim()))
                {
                    myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                    if (ProxyChange >= 16)
                        ProxyChange = 0;
                }
                
                myWebRequest.UserAgent = "Mozilla/5.0(Windows NT 10.0; Win64; x64) AppleWebKit/537.36(KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36";
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
                myWebRequest.Referer = url;
                myWebRequest.Headers["Accept-encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9,es;q=0.8";
                myWebRequest.ContentType = "application/json; charset=utf-8";
                myWebRequest.Headers["authority"] = "www.gmpartsoutlet.net";
                myWebRequest.Headers["sec-fetch-dest"] = "empty";
                myWebRequest.Headers["sec-fetch-mode"] = "cors";
                myWebRequest.Headers["sec-fetch-site"] = "same-origin";
                myWebRequest.Headers["Upgrade-Insecure-Requests"] = "1";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";

                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;

                using (var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse())
                {
                    Stream responseStream = myWebResponse.GetResponseStream();
                    StreamReader myWebSource = new StreamReader(responseStream, Encoding.Default);
                    myPageSource = myWebSource.ReadToEnd();
                }
                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(1000);
                    goto retryContent;
                }
                return HtmlHelper.ReturnFormatedHtml(myPageSource);
            }
            catch (Exception ex)
            {   
              //CommonUtility.ErrorLog(ex, "Url : " + url, "GetSource");      
            }
            return myPageSource;
        }

        public string _GetJsonSource(string url, string cookieeee)
        {
            int retry = 0;
            string myPageSource = "";
        retryContent:
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                HttpWebRequest myWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                myWebRequest.KeepAlive = true;
                myWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                myWebRequest.Method = WebRequestMethods.Http.Get;
                myWebRequest.Proxy = SetProxySevere(++ProxyChange);
                //myWebRequest.UserAgent = UserAgentArr[++UsrAgnt];
                //if (UsrAgnt >= UserAgentArr.Length - 2)
                //    UsrAgnt = 0;
                myWebRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
                myWebRequest.Headers["Accept-Language"] = "en-US,en;q=0.9";
                myWebRequest.Headers["Accept-Encoding"] = "gzip, deflate, br";
                myWebRequest.Headers["Cookie"] = cookieeee;
                myWebRequest.Host = "www.fiverr.com";
                myWebRequest.Headers["X-Requested-With"] = "XMLHttpRequest";
                myWebRequest.Timeout = Timeout.Infinite;
                myWebRequest.AllowAutoRedirect = false;
                myWebRequest.KeepAlive = true;
                HttpWebResponse myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();
                StreamReader myWebSource = new StreamReader(responseStream, Encoding.UTF8);
                myPageSource = myWebSource.ReadToEnd();
                myWebResponse.Close();
                responseStream.Close();
                if (myPageSource == "" && retry < retryCount)
                {
                    retry++;
                    Thread.Sleep(2000);
                    goto retryContent;
                }
            }
            catch (Exception ex)
            {
                goto retryContent;
            }
            return myPageSource;
        }

        public WebProxy SetProxySevere(int ChangeCount)
        {
            WebProxy setproxy = new WebProxy("world.proxymesh.com", 31280);
            if (ChangeCount <= 4)
            {
                setproxy = new WebProxy("us.proxymesh.com", 31280);
            }
            else
            {
                if (ChangeCount <= 8)
                {
                    setproxy = new WebProxy("us-il.proxymesh.com", 31280);
                }
                else
                {
                    if (ChangeCount <= 12)
                    {
                        setproxy = new WebProxy("us-fl.proxymesh.com", 31280);
                    }
                    else
                    {
                        if (ChangeCount <= 16)
                        {
                            setproxy = new WebProxy("us-ca.proxymesh.com", 31280);
                        }
                        else
                        {
                            setproxy = new WebProxy("us-dc.proxymesh.com", 31280);
                            ChangeCount = 0;
                        }
                    }
                }
            }

            return setproxy;
        }

        bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }
    }

}
