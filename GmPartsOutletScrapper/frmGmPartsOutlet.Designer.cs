﻿namespace GmPartsOutletScrapper
{
    partial class frmGmPartsOutlet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGmStart = new System.Windows.Forms.Label();
            this.lblGmPartsEnd = new System.Windows.Forms.Label();
            this.lblInit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblGmStart
            // 
            this.lblGmStart.AutoSize = true;
            this.lblGmStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGmStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGmStart.Location = new System.Drawing.Point(34, 79);
            this.lblGmStart.Name = "lblGmStart";
            this.lblGmStart.Size = new System.Drawing.Size(135, 16);
            this.lblGmStart.TabIndex = 0;
            this.lblGmStart.Text = "lblGmPartsOutletStart";
            // 
            // lblGmPartsEnd
            // 
            this.lblGmPartsEnd.AutoSize = true;
            this.lblGmPartsEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGmPartsEnd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblGmPartsEnd.Location = new System.Drawing.Point(34, 108);
            this.lblGmPartsEnd.Name = "lblGmPartsEnd";
            this.lblGmPartsEnd.Size = new System.Drawing.Size(132, 16);
            this.lblGmPartsEnd.TabIndex = 1;
            this.lblGmPartsEnd.Text = "lblGmPartsOutletEnd";
            // 
            // lblInit
            // 
            this.lblInit.AutoSize = true;
            this.lblInit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblInit.Location = new System.Drawing.Point(37, 43);
            this.lblInit.Name = "lblInit";
            this.lblInit.Size = new System.Drawing.Size(124, 16);
            this.lblInit.TabIndex = 2;
            this.lblInit.Text = "lblGmPartsOutletInit";
            // 
            // frmGmPartsOutlet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(593, 175);
            this.Controls.Add(this.lblInit);
            this.Controls.Add(this.lblGmPartsEnd);
            this.Controls.Add(this.lblGmStart);
            this.Name = "frmGmPartsOutlet";
            this.Text = "GmPartsOutlet";
            this.Shown += new System.EventHandler(this.frmGmPartsOutlet_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblGmStart;
        private System.Windows.Forms.Label lblGmPartsEnd;
        private System.Windows.Forms.Label lblInit;
    }
}

