﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GmPartsOutletScrapper.ScrapHelper;
using ScrapingHelp;
using System.Diagnostics;
using System.Threading;

namespace GmPartsOutletScrapper
{
    public partial class frmGmPartsOutlet : Form
    {
        GmPartsOutletScrapper.ScrapHelper.ScrapHelper _scrapeHelper = new GmPartsOutletScrapper.ScrapHelper.ScrapHelper(Convert.ToInt16(ConfigurationSettings.AppSettings["retryCount"].ToString()));
        GmPartsOutletBAL.GmPartsOutletBussiness _gmPartsOutlet = new GmPartsOutletBAL.GmPartsOutletBussiness();
        string gmpartsoutletlink = ConfigurationSettings.AppSettings["PartsLinkTable"].ToString();
        string gmpartsoutletdata = ConfigurationSettings.AppSettings["PartsDataTable"].ToString();
        string vehiclefitment = ConfigurationSettings.AppSettings["PartsVehicleFitment"].ToString();
        string PartsName = string.Empty;
        public frmGmPartsOutlet()
        {
            InitializeComponent();

        }
        private void frmGmPartsOutlet_Shown(object sender, EventArgs e)
        {
            Thread.Sleep(100);
            setStarupParameter();
        }
        public void setStarupParameter()
        {
            string url = string.Empty;
            try
            {
                PartsName = Environment.GetCommandLineArgs()[1].Trim(' ');
                url = string.Format("https://www.gmpartsoutlet.net/search?search_str={0}", PartsName);
                lblInit.ForeColor = Color.Green;
                lblInit.Text = "GM Parts Outlet Script Initialization Completed : " + DateTime.Now;
                lblInit.Refresh();
                GetPartsUrl(url, PartsName);
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                _gmPartsOutlet.ErrorLog(ex, url, "setStarupParameter", PartsName, "");
            }
        }

        public int GetTotalPage(string Count)
        {
            int pages = 0;
            if (Count.Contains(","))
            {
                Count = Count.Replace(",", "");
            }
            int totalCount = Convert.ToInt32(Count);
            if (totalCount > 18)
            {
                string result = Math.Round(Convert.ToDecimal(totalCount) / 18, 2).ToString();
                string[] arr = result.Split('.');
                if (Convert.ToInt16(arr[1]) > 1)
                {
                    pages = Convert.ToInt32(Math.Round(Convert.ToDecimal(totalCount) / 18, 1)) + 1;
                }
                else
                {
                    pages = Convert.ToInt32(Math.Round(Convert.ToDecimal(totalCount) / 18, 1));
                }
            }
            return pages;
        }

        public void GetPartsUrl(string url, string partName)
        {
            lblGmStart.ForeColor = Color.Green;
            lblGmStart.Text = "GM Parts Outlet Script (" + PartsName + ") Start : " + DateTime.Now;
            lblGmStart.Refresh();
            string CSVPath = string.Empty, FileName = string.Empty;
            StringBuilder sbGmPartsOutletLink = new StringBuilder();
            try
            {
                string content = _scrapeHelper.GetSource(url, "");
                string totalCount = HtmlHelper.ReturnValue(content, "<span class=\"result-count\">", "</span>");
                int Pages = GetTotalPage(totalCount);
                if (Pages > 1)
                {
                    for (int i = 1; i <= Pages; i++)
                    {
                        url = string.Format("https://www.gmpartsoutlet.net/search?search_str={0}&page={1}", PartsName, i);
                        content = _scrapeHelper.GetSource(url, "");

                        string[] contentPerPage = content.Split(new string[] { "<div class=\"catalog-product row\">" }, StringSplitOptions.None);
                        for (int ii = 1; ii < contentPerPage.Length; ii++)
                        {
                            string PartNumber = HtmlHelper.ReturnValue(contentPerPage[ii], "<strong>Part Number:</strong>", ">", "</a>");
                            string MainUrl = HtmlHelper.ReturnValue(contentPerPage[ii], "<strong>Part Number:</strong>", "<a href=\"", "\">");
                            string URL = MainUrl != "" ? string.Format("https://www.gmpartsoutlet.net{0}", MainUrl) : "";

                            //_gmPartsOutlet.InsertGmPartOutletLink(PartNumber, PartsName, URL);

                            sbGmPartsOutletLink.AppendLine(string.Format("0|" + PartNumber + "|" + PartsName + "|" + URL + "|" + i + "|" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "|" + "1|"));
                        }
                    }
                    CSVPath = ConfigurationSettings.AppSettings["CSVPartOutlet"].ToString() + partName;
                    if (!System.IO.Directory.Exists(CSVPath))
                    {
                        System.IO.Directory.CreateDirectory(CSVPath);
                    }
                    FileName = partName + "_" + DateTime.Now.ToString("ddMMyyy") + "_" + Guid.NewGuid().ToString() + ".csv";
                    System.IO.File.WriteAllText(CSVPath + "\\" + FileName, sbGmPartsOutletLink.ToString());
                }

                //Insert Bulk Data
                int result = _gmPartsOutlet.InsertBulkData(CSVPath + "\\" + FileName, gmpartsoutletlink);
                if (result == 1)
                {
                    // _gmPartsOutlet.DeleteCSVFile(CSVPath + "\\" + FileName);
                }
                //Get Gm-Parts-Outlet URL 
                DataTable dtPartsMainLink = _gmPartsOutlet.GetGmPartsOutletLink(PartsName, gmpartsoutletlink);
                GetGmPartsOutletData(dtPartsMainLink, PartsName, gmpartsoutletdata);
            }
            catch (Exception ex)
            {
                _gmPartsOutlet.ErrorLog(ex, url, "GetPartsUrl", PartsName, "");
            }
        }

        public void GetGmPartsOutletData(DataTable dtPartsLink, string PartName, string gmpartsoutletdata)
        {
            try
            {
                string imageUrl1 = string.Empty, imageUrl2 = string.Empty, imageUrl3 = string.Empty, imageUrl4 = string.Empty, imageUrl5 = string.Empty, imageUrl6 = string.Empty;
                string imgs = string.Empty;
                string[] images = new string[] { };
                string partsLink = string.Empty, partNumber = string.Empty;
                int retry = 0;
            //StringBuilder sbGmPartsOutletData = new StringBuilder();
            //string CSVPath = ConfigurationSettings.AppSettings["CSVPartOutlet"].ToString() + PartName;
            //if (!System.IO.Directory.Exists(CSVPath))
            //{
            //    System.IO.Directory.CreateDirectory(CSVPath);
            //}
            //string FileName = PartName + "_Data_" + DateTime.Now.ToString("ddMMyyy") + "_" + Guid.NewGuid().ToString() + ".csv";
            retryProcessed:
                if (dtPartsLink.Rows.Count > 0)
                {
                    foreach (DataRow drParts in dtPartsLink.Rows)
                    {
                        partsLink = drParts["Url"]!=null?drParts["Url"].ToString():"";
                        partNumber = drParts["PartNumber"]!=null? drParts["PartNumber"].ToString():"";
                        int LinkID = Convert.ToInt32(drParts["ID"].ToString());
                        string partContent = _scrapeHelper.GetSource(partsLink, "");
                        if (partContent != "")
                        {
                            try
                            {
                                string SKU = HtmlHelper.ReturnValue(partContent, "<span class=\"list-label\">SKU:</span>", "<span class=\"list-value sku-display\">", "</span>") != "" ? HtmlHelper.ReturnValue(partContent, "<span class=\"list-label\">SKU:</span>", "<span class=\"list-value sku-display\">", "</span>") : "";
                                string Replaces = HtmlHelper.ReturnValue(partContent, "<span class=\"list-label\">Replaces:</span>", "<span class=\"list-value\">", "</span>") != "" ? HtmlHelper.ReturnValue(partContent, "<span class=\"list-label\">Replaces:</span>", "<span class=\"list-value\">", "</span>") : "";
                                images = partContent.Split(new string[] { "<div class=\"secondary-images-wrapper owl-small\">" }, StringSplitOptions.None);
                                if (images.Length > 1)
                                {
                                    if (!string.IsNullOrEmpty(images[1]))
                                    {
                                        string imggs = images[1].ToString();
                                        string[] arr = HtmlHelper.CollectUrl(imggs, "", "src=\"", "\"/></a></li>");
                                        imageUrl1 = arr[0] != "" ? arr[0] : "";
                                        if (arr.Length > 1)
                                        {
                                            imageUrl2 = arr[1] != "" ? arr[1] : "";
                                        }
                                        if (arr.Length > 2)
                                        {
                                            imageUrl3 = arr[2] != "" ? arr[2] : "";
                                        }
                                        if (arr.Length > 3)
                                        {
                                            imageUrl4 = arr[3] != "" ? arr[3] : "";
                                        }
                                        if (arr.Length > 4)
                                        {
                                            imageUrl5 = arr[4] != "" ? arr[4] : "";
                                        }
                                        if (arr.Length > 5)
                                        {
                                            imageUrl6 = arr[5] != "" ? arr[5] : "";
                                        }
                                    }
                                }
                                if (images.Length == 1)
                                {
                                    imageUrl1 = HtmlHelper.ReturnValue(partContent, "<div class=\"main-image\">", "<a href=\"", "\"") != "" ? string.Format("https://www.gmpartsoutlet.net{0}", HtmlHelper.ReturnValue(partContent, "<div class=\"main-image\">", "<a href=\"", "\"")) : "";
                                }
                                //sbGmPartsOutletData.AppendLine("0|"+LinkID +"|"+ partNumber + "|" + SKU + "|" + Replaces + "|" + imageUrl1 + "|" + "|" + imageUrl2 + "|" + imageUrl3 + "|" + imageUrl4 + "|" + imageUrl5 + "|" + imageUrl6 + "|" + DateTime.Now.ToString("yyyy-MM-dd") + "|");

                                int DataID = _gmPartsOutlet.InsertGmPartOutletData(SKU, Replaces, imageUrl1, imageUrl2, imageUrl3, imageUrl4, imageUrl5, imageUrl6, partNumber, LinkID, gmpartsoutletdata);
                                GetVehicleFitment(partContent, partNumber, PartName, DataID);
                            }
                            catch (Exception ex)
                            {
                                int retryError = 0;
                                if(retryError<=5)
                                {
                                    goto retryProcessed;
                                    retryError++;
                                }
                                else
                                {
                                    _gmPartsOutlet.ErrorLog(ex, partsLink, "GetGmPartsOutletData", PartName, partNumber);
                                }
                                
                            }
                            continue;
                        }
                    }
                    //System.IO.File.WriteAllText(CSVPath + "\\" + FileName, sbGmPartsOutletData.ToString());

                    //int result = _gmPartsOutlet.InsertBulkData(CSVPath + "\\" + FileName, "gmpartsoutletdata");
                    //if (result == 1)
                    //{
                    //   // _gmPartsOutlet.DeleteCSVFile(CSVPath + "\\" + FileName);
                    //}
                    dtPartsLink = new DataTable();
                    dtPartsLink = _gmPartsOutlet.GetGmPartsOutletLink(PartsName, gmpartsoutletlink);
                    
                    if(dtPartsLink.Rows.Count>0)
                    {
                        if(retry<=5)
                        {
                            retry++;
                            goto retryProcessed;
                        }
                    }
                    lblGmPartsEnd.ForeColor = Color.Green;
                    lblGmPartsEnd.Text = "GM Parts Outlet Script (" + PartsName + ") End : " + DateTime.Now;
                    lblGmPartsEnd.Refresh();
                }
            }
            catch (Exception ex)
            {
                _gmPartsOutlet.ErrorLog(ex, "", "GetGmPartsOutletData", PartName, "");
            }
        }

        public void GetVehicleFitment(string partsContent, string PartNumber, string PartName, int DataID)
        {
            try
            {
                string[] trNodes = new string[] { };
                string[] trNodesMore = new string[] { };
                string Make = string.Empty, BodyAndTrim = string.Empty, EngineAndTransmission = string.Empty;

                StringBuilder sbVehicleFitment = new StringBuilder();
                string CSVPath = ConfigurationSettings.AppSettings["CSVPartOutlet"].ToString() + PartsName + "_" + PartNumber;
                if (!System.IO.Directory.Exists(CSVPath))
                {
                    System.IO.Directory.CreateDirectory(CSVPath);
                }
                string FileName = PartName + "_" + PartNumber + "_" + DateTime.Now.ToString("ddMMyyy") + "_" + Guid.NewGuid().ToString() + ".csv";

                if (partsContent != "")
                {
                    trNodes = partsContent.Split(new string[] { "<tr class=\"fitment-row\">" }, StringSplitOptions.None);

                    if (trNodes.Length > 0)
                    {
                        for (int i = 1; i < trNodes.Length; i++)
                        {
                            Make = HtmlHelper.ReturnValue(trNodes[i], "<td class=\"fitment-year-make-model\">", "</td>");
                            BodyAndTrim = HtmlHelper.ReturnValue(trNodes[i], "<td class=\"fitment-trim\">", "</td>");
                            EngineAndTransmission = HtmlHelper.ReturnValue(trNodes[i], "<td class=\"fitment-engine\">", "</td>");

                            //_gmPartsOutlet.InsertVehicleFitment(Make, BodyAndTrim, EngineAndTransmission, PartNumber);
                            sbVehicleFitment.AppendLine("0|" + DataID + "|" + PartNumber + "|" + Make + "|" + BodyAndTrim + "|" + EngineAndTransmission + "|" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "|");
                        }
                    }
                    int haveMoreNodes = HtmlHelper.ReturnValue(partsContent, "<span class=\"closed-count\">", "</span>") != "" ? Convert.ToInt32(HtmlHelper.ReturnValue(partsContent, "<span class=\"closed-count\">", "</span>").Replace("more", "")) : 0;
                    if (haveMoreNodes > 0)
                    {
                        trNodesMore = partsContent.Split(new string[] { "<tr class=\"fitment-row fitment-hidden\">" }, StringSplitOptions.None);
                        if (trNodesMore.Length > 0)
                        {
                            for (int i = 1; i < trNodesMore.Length; i++)
                            {
                                Make = HtmlHelper.ReturnValue(trNodesMore[i], "<td class=\"fitment-year-make-model\">", "</td>");
                                BodyAndTrim = HtmlHelper.ReturnValue(trNodesMore[i], "<td class=\"fitment-trim\">", "</td>");
                                EngineAndTransmission = HtmlHelper.ReturnValue(trNodesMore[i], "<td class=\"fitment-engine\">", "</td>");

                                //_gmPartsOutlet.InsertVehicleFitment(Make, BodyAndTrim, EngineAndTransmission, PartNumber);
                                sbVehicleFitment.AppendLine("0|" + DataID + "|" + PartNumber + "|" + Make + "|" + BodyAndTrim + "|" + EngineAndTransmission + "|" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "|");
                            }
                        }
                    }

                    System.IO.File.WriteAllText(CSVPath + "\\" + FileName, sbVehicleFitment.ToString());

                    int result = _gmPartsOutlet.InsertBulkData(CSVPath + "\\" + FileName, vehiclefitment);
                    if (result == 1)
                    {
                        // _gmPartsOutlet.DeleteCSVFile(CSVPath + "\\" + FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                _gmPartsOutlet.ErrorLog(ex, "", "GetVehicleFitment", PartName, PartNumber);
            }
        }

    }
}
